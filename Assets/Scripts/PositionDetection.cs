﻿using UnityEngine;
using System.Collections;

public class PositionDetection : MonoBehaviour {

	/*public Transform Body;
	public Transform Head;

	public Transform LeftHand;
	public Transform LeftArm;
	public Transform LeftLeg;
	public Transform LeftFoot;

	public Transform RightHand;
	public Transform RightArm;
	public Transform RightLeg;
	public Transform RightFoot;*/

	public Collider root;
	public float passLimitDir = 0.1f;
	public float passLimitPos = 0.2f;

	private Limb[] limbs;
	private Vector3[] directions;
	private Vector3[] positions;
	private bool inited = false;

	// Use this for initialization
	void Start () {

	}
 	
	void OnGUI()
	{
		if (inited)
		{
			int dirSuccess = 0;
			int posSuccess = 0;
			for (int i = 0; i < limbs.Length; i++){
				if ((GetComparisonDir(limbs[i]) - directions[i]).magnitude <= passLimitDir)
					dirSuccess++;
				if ((GetComparisonPos(limbs[i]).normalized - positions[i].normalized).magnitude <= passLimitPos)
					posSuccess++;
			}
			
			string s;
			if (posSuccess == limbs.Length)
				s = "Posture valid!";
			else
				s = /*"Dir: " + dirSuccess.ToString() + " / " + limbs.Length.ToString() +*/ "Posture: " + posSuccess.ToString() + " / " + limbs.Length.ToString(); 
			GUI.Label(new Rect(20f, 20f, 300f, 100f), s);
		}


	}

	// Update is called once per frame
	/*void Update () {

	}*/

	public void Print()
	{
		if (inited)
		{
			for (int i = 0; i < limbs.Length; i++)
			{
				Debug.Log(limbs[i].name + ",  Position:" + positions[i].ToString() + " / " + GetComparisonPos(limbs[i]).ToString() + " ,Direction:" + directions[i].ToString() + " / " + GetComparisonPos(limbs[i]).ToString());
			}
		}
	}

	public void Save()
	{
		limbs = root.GetComponentsInChildren<Limb>();
		directions = new Vector3[limbs.Length];
		positions = new Vector3[limbs.Length];
		
		for (int i = 0; i < limbs.Length; i++)
		{
			positions[i] = GetComparisonPos(limbs[i]);
			directions[i] = GetComparisonDir(limbs[i]);
		}

		inited = true;

		Debug.Log("Posture saved.");
	}

	private Vector3 GetComparisonPos(Limb limb)
	{
		return limb.parentNode.transform.InverseTransformPoint(limb.controlNode.transform.position);
	}

	private Vector3 GetComparisonDir(Limb limb)
	{
		return limb.parentNode.transform.InverseTransformDirection(limb.parentNode.transform.position - limb.controlNode.transform.position).normalized;
	}

	/*void ToAllChildren(Transform parent)
	{

		
		foreach (Transform child in parent) {
			if (!parent.Equals(child))
			{
				if (child.collider != null)
				{

				}

				ToAllChildren(child);
			}
		}
	}*/

}
