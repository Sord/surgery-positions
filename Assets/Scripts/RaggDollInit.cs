﻿using UnityEngine;
using System.Collections;

public class RaggDollInit : MonoBehaviour {
	
	public float drag = 0.0f;
	public float angularDrag = 0.0f;
	
	void Start () {
		InitDragDoll();
	}
	
	void InitDragDoll()
	{
		GetChildren(this.transform);
	}
	
	void GetChildren(Transform parent)
	{
		Rigidbody body = parent.rigidbody;
		if (body != null)
		{
			InitRigidbody(body);
		}
		
		foreach (Transform child in parent) {
			if (!parent.Equals(child))
			{
				GetChildren(child);
			}
	    }
	}
	
	void InitRigidbody(Rigidbody body)
	{
		body.drag = drag;
		body.angularDrag = angularDrag;
	}
}
