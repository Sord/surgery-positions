﻿using UnityEngine;
using System.Collections;

public class Limb : MonoBehaviour {

	public GameObject controlNode;
	public GameObject parentNode;

	void Start()
	{
		if (controlNode == null)
			controlNode = transform.GetChild(0).gameObject;
		if (parentNode == null)
			parentNode = transform.parent.gameObject;
	}

}
