﻿using UnityEngine;
using System.Collections;

public class BedPart : MonoBehaviour {

	public float maxAngle = 90.0f;
	public float minAngle = -90.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TurnTo(Vector3 localGrabPoint, Vector3 worldTargetPoint)
	{
		Quaternion orginalRotation = transform.localRotation;
		transform.rotation = Quaternion.identity;
		Vector3 localTarget = transform.InverseTransformPoint(worldTargetPoint);
		
		Quaternion test;
		Quaternion best = Quaternion.identity;
		float bestVal = float.MaxValue;
		for (float i = minAngle; i <= maxAngle; i++)
		{
			test = Quaternion.Euler( new Vector3(i, 0.0f, 0.0f));
			float val = Vector3.Distance((test * localGrabPoint), localTarget);
			if (val < bestVal)
			{
				bestVal = val;
				best = test;
			}
		}

		Quaternion target = best * Quaternion.Euler(90.0f, 0.0f, 0.0f);
		//transform.localRotation = Quaternion.Lerp(transform.localRotation, best * Quaternion.Euler(90.0f, 0.0f, 0.0f), 0.05f);
		transform.localRotation = Quaternion.Lerp(orginalRotation, target, 0.1f);
	}
}
