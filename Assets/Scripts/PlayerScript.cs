﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerScript : MonoBehaviour {	
	
	//public GameObject patient;
	public float speed;
	public float gravity;
	public float grabPowerFactor;
	public float grabMaxPower;
	public float bodyMomentum;
	public float scrollSensitivity;
	public float yfactor;
	public float rayGrabRadius = 0.1f;

	public PositionDetection detector;

	public MeshRenderer crosshairRenderer;

	public Color crosshairActiveColor = Color.red;
	public Color crosshairColor = Color.white;

	delegate void GameObjectDelegate(GameObject obj);
	bool mouseLock;
	
	Vector3 grab = new Vector3(0.0f, 0.0f, 1.0f);
	
	GameObject grabbed = null;
	Vector3 grabPoint;
	float grabDistance;
	int grabableMask;
	
	
	void Start () {
		mouseLock = false;
		grabableMask = 1 << LayerMask.NameToLayer("Grabable");
		MouseLock();
	}	

	
	void Update () {
		
		CharacterController cc = GetComponent<CharacterController>();
		Camera cam = Camera.main;
		
		Vector3 movement = new Vector3();
		movement.z = Input.GetAxis("Vertical");
		movement.x = Input.GetAxis("Horizontal");		
		movement = transform.TransformDirection(movement) * speed;		
		movement.y = -gravity;
		cc.Move(movement);
		
		cam.transform.Rotate(-Input.GetAxis("Mouse Y"), 0.0f, 0.0f);
		cc.transform.Rotate(0.0f, Input.GetAxis("Mouse X"), 0.0f);
		
		if (Input.GetMouseButtonDown(1))
		{		
			MouseLock();
		}

		Ray ray = cam.ScreenPointToRay(new Vector2(Screen.width / 2.0f, Screen.height / 2.0f));
		RaycastHit hit;
		bool raycastHit = Physics.SphereCast(ray, rayGrabRadius, out hit, 100.0f, grabableMask);

		bool showActiveColor = false;

		if (Input.GetMouseButtonDown(0))
		{	
			if (raycastHit)
			{
				showActiveColor = true;
				
				if (hit.rigidbody != null)
				{
					grabbed = hit.rigidbody.gameObject;
					grabPoint = grabbed.transform.InverseTransformPoint(hit.point);
					grabDistance = hit.distance;

					if (grabbed.tag == "bed")
					{
					
					}
					else if (grabbed.tag == "limb")
					{
						//ToAllChildren(grabbed.transform, new GameObjectDelegate(SetDynamic));
					}
				}

			}	
		}
		else if (Input.GetMouseButton(0))
		{
			if (grabbed != null)
			{
				showActiveColor = true;

				float scroll = Input.GetAxis("Mouse ScrollWheel");
				grabDistance += scroll * scrollSensitivity;

				Vector3 worldGrabber = ray.GetPoint(grabDistance);

				if (grabbed.tag == "bed")
				{
					BedPart bedPart = grabbed.GetComponent<BedPart>();
					bedPart.TurnTo(grabPoint, worldGrabber);
				}
				else if (grabbed.tag == "limb")
				{
					Vector3 worldGrabbed = grabbed.transform.TransformPoint(grabPoint);
					Vector3 dif = worldGrabber - worldGrabbed;
					Vector3 force = dif * grabPowerFactor;
					float forceMagn = force.magnitude;
					if (forceMagn > grabMaxPower)
					{
						force *= grabMaxPower / forceMagn;
					}
					force.y *= yfactor;
					grabbed.rigidbody.AddForce(force, ForceMode.Acceleration);
				}
				else if (grabbed.tag == "body")
				{
					grabbed.rigidbody.AddRelativeTorque(new Vector3(0f, bodyMomentum, 0f), ForceMode.Acceleration);
				}
			}
			
			//Debug.Log("Hand world: " + worldGrab.ToString());
			//Debug.Log("Dif: " + dif.ToString());
		}
		else if (Input.GetMouseButtonUp(0))
		{
			if (grabbed != null)
			{
				showActiveColor = true;

				if (grabbed.tag == "bed")
				{
					
				}
				else if (grabbed.tag == "limb")
				{
					//ToAllChildren(grabbed.transform, new GameObjectDelegate(SetKinematic));
				}

				grabbed = null;
			}
		}
		else
		{
			showActiveColor = raycastHit;
		}

		if (showActiveColor)
			crosshairRenderer.material.color = crosshairActiveColor;
		else
			crosshairRenderer.material.color = crosshairColor;

		if (Input.GetKeyDown(KeyCode.Space))
		{
			detector.Print();
		}
		if (Input.GetKeyDown(KeyCode.LeftControl))
		{
			detector.Save();
		}
		if (Input.GetKeyDown(KeyCode.R))
		{
			Application.LoadLevel (Application.loadedLevelName);
		}

	}

	void SetKinematic(GameObject go)
	{
		Rigidbody body = go.rigidbody;
		if (body != null)
			body.isKinematic = true;
	}

	void SetDynamic(GameObject go)
	{
		Rigidbody body = go.rigidbody;
		if (body != null)
			body.isKinematic = false;
	}

	void ToAllChildren(Transform parent, GameObjectDelegate del)
	{
		del(parent.gameObject);

		foreach (Transform child in parent) {
			if (!parent.Equals(child))
			{
				ToAllChildren(child, del);
			}
		}
	}
	
	void MouseLock()
	{
		if (mouseLock)
		{
			Screen.lockCursor = false;
			Screen.showCursor = true;
			mouseLock = false;	
		}
		else
		{			
			Screen.lockCursor = true;
			Screen.showCursor = false;
			mouseLock = true;
		}
	}
}
